<?php

use App\Http\Controllers\MovieController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Public routes for registering and login
Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);

// Private routes for users w/ API token
Route::group(['middleware' => ['auth:sanctum', 'throttle']], function() {
    Route::resource('movies', MovieController::class); // list all movies & show all info for a selected one
    Route::get('/movies/search/{title}', [MovieController::class, 'searchByTitle']);
    Route::get('/movies/show/{category}', [MovieController::class, 'showByCategory']);
});