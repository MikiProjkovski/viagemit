<?php

namespace Database\Factories;

use App\Models\Movie;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference_code' => Str::random(4),
            'title' => $this->faker->unique()->sentence($nbWords = 6, $variableNbWords = true),
            'category' => $this->faker->randomElement($array = array ('Action','Comedy','Thriller', 'Horror', 'Drama')),
            'image_url' => $this->faker->imageUrl($width = 640, $height = 480),
            'production_year' => $this->faker->year($max = 'now')
        ];
    }
}
